var express = require('express');
var router = express.Router();

var friendsCount = require('.././friendsCount');
var friends = require('.././friends');
var friendsRequest = require('.././friendsRequest');
var friendsRequestCount = require('.././friendsRequestCount');


/**
 * @api {get} /api/friends/getCount/:id Общее количество друзей.
 * @apiName getCount
 * @apiGroup Friends
 *
 * @apiPermission todo
 *
 *
 * @apiParam {Number} id Account ID.
 *
 * @apiSuccess {Number} idAccount Account ID.
 * @apiSuccess {Number} count  Количество друзей.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "idAccount": 1,
 *       "count": 100
 *     }
 *
 * @apiError AccountNotFound The id of the Account was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "AccountNotFound"
 *     }
 *
 *
 */
router.get('/getCount/:id', function(req, res, next) {
    res.send('friends.getCount' + friendsCount.getCount(1));
});


/**
 * @api {get} /api/friends/get/:id Список друзей.
 * @apiName get
 * @apiGroup Friends
 *
 * @apiDescription Возвращает список друзей пользователя. <br>
 * Максимальное количество друзей - 100. Этот принцип отличается от других социальных сетей, что не просто ставим всех в друзья, а действительно выбираем тех, с кем общаемся.<br>
 * <u>todo: Определиться с составом полей.<br></u>
 *
 * @apiPermission todo
 *
 *
 * @apiParam {Number} id Account ID.
 * @apiParam {Number{0-100}} count Количество друзей, которое нужно вернуть. (по умолчанию – 0 - все друзья). Максимум 100
 * @apiParam {Number} offset Смещение, необходимое для выборки определенного подмножества друзей.
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "count": 3,
 *       "items": [
 *          {id: 1, name: "Сергей"},
 *          {id: 2, name: "Иван"},
 *          {id: 3, name: "Ольга"}
 *       ]
 *     }
 *
 * todo: Определиться с возможными ошибками.
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "AccountNotFound",
 *       "count": 3,
 *       "items": []
 *     }
 *
 *
 */
router.get('/get/:id', function(req, res){
    friends.getList();
    res.send('test');
});


/**
 * @api {put} /api/friends/delete Удалить дружбку
 * @apiName delete
 * @apiGroup Friends
 *
 * @apiDescription Удаляет связь (дружбу) между двумя пользователями.
 *
 * @apiPermission todo
 *
 *
 * @apiParam {Number} firstIdAccount
 * @apiParam {Number} secondIdAccount
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true
 *     }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "success": false
 *       "error": "",
 *     }
 *
 *
 */
router.put('/delete/', function(req, res){
    res.send('delete');
});

/**
 * @api {get} /api/friends/areFriends Являются друзьями.
 * @apiName delete
 * @apiGroup Friends
 *
 * @apiDescription Проверяет, являются ли указанные пользователи друзьями.
 *
 * @apiParam {Number} firstIdAccount
 * @apiParam {Number} secondIdAccount
 *
 * @apiPermission todo
 *
 * @apiSuccess {Number} areFriends 1 - друзья, 0 - не друзья, 2 - есть запрос на друзья.
 *
 * todo:  в будующем вернеться информации о подписке.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "areFriends": true
 *     }
 *
 *
 */
router.get('/areFriends/', function(req, res){
    res.send('areFriends');
});


/**
 * @api {put} /api/friends/sendRequestToBeFriend Запрос на друзья.
 * @apiName sendRequestToBeFriend
 * @apiGroup Friends
 *
 * @apiDescription Отправляет запрос пользователю на добавление в друзья.
 *
 * @apiParam {Number} senderIdAccount Код пользователя, который направляет предложение дружбы.
 * @apiParam {Number} targetIdAccount Код пользователя, которому направляют предложение дружбы.
 * @apiParam {String} text Текст сопроводительного сообщения для заявки на добавление в друзья. Максимальная длина сообщения — 200 символов..
 *
 * @apiPermission todo
 *
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "areFriends": true
 *     }
 *
 *
 */
router.put('/sendRequestToBeFriend/', function(req, res){
    res.send('areFriends');
});

/**
 * @api {get} /api/friends/confirmRequestToBeFriend Принятие дружбы.
 * @apiName sendRequestToBeFriend
 * @apiGroup Friends
 *
 * @apiDescription Принятия предложения дружбы.
 *
 * @apiParam {Number} idAccount Код пользователя, которому было направлено предложение дружбы и который принимает это предложение.
 * @apiParam {Number} targetIdAccount Код пользователя, который направил предложение дружбы.
 *
 * @apiPermission todo
 *
 *
 */
router.put('/confirmRequestToBeFriend/', function(req, res){
    res.send('areFriends');
});

/**
 * @api {put} /api/friends/rejectRequestToBeFriend Отклонение дружбы.
 * @apiName confirmRequestToBeFriend
 * @apiGroup Friends
 *
 * @apiDescription Метод служит для отклонения запроса на дружбу.
 *
 * @apiParam {Number} idAccount Код пользователя, которому был направлен запрос и который его отклоняет.
 * @apiParam {Number} targetIdAccount Код пользователя, который направил предложение дружбы.
 *
 * @apiPermission todo
 *
 *
 */
router.get('/rejectRequestToBeFriend/', function(req, res){
    res.send('areFriends');
});



/**
 * @api {get} /api/friends/getRequestsToBeFriend Список запросов.
 * @apiName getRequestsToBeFriend
 * @apiGroup Friends
 *
 * @apiDescription Возвращает информацию о полученных или отправленных заявках на добавление в друзья для текущего пользователя. .
 *
 * @apiParam {Number} idAccount Идентификатор пользователя.
 * @apiParam {Number} out 0 — возвращать полученные заявки в друзья (по умолчанию), 1 — возвращать отправленные пользователем заявки. флаг, может принимать значения 1 или 0.
 * @apiParam {Number} count максимальное количество заявок на добавление в друзья, которые необходимо получить (не более'''1000). По умолчанию — 100''.
 * @apiParam {Number} offset смещение, необходимое для выборки определенного подмножества заявок на добавление в друзья.
 *
 * @apiPermission todo
 *
 *
 */
router.get('/getRequestsToBeFriend/', function(req, res){
    res.send('areFriends');
});


/**
 * @api {get} /api/friends/getCountRequests Количество входящих заявок.
 * @apiName getCountRequests
 * @apiGroup Friends
 *
 * @apiDescription Возвращает количество входящих заявок.
 *
 * @apiParam {Number} idAccount Идентификатор пользователя.
 *
 * @apiPermission todo
 *
 *
 */
router.get('/getCountRequests/', function(req, res){
    res.send('getCountRequests');
});

/**
 * @api {put} /api/friends/deleteCountRequests Сброс заявок.
 * @apiName deleteCountRequests
 * @apiGroup Friends
 *
 * @apiDescription Сбрасывает на 0 счетчик входящих заявок. .
 *
 * @apiParam {Number} idAccount Идентификатор пользователя.
 *
 * @apiPermission todo
 *
 *
 */
router.get('/deleteСountRequests/', function(req, res){
    res.send('deleteСountRequests');
});


/**
 * @api {get} /api/friends/getOnline Друзья онлайн.
 * @apiName getOnline
 * @apiGroup Friends
 *
 * @apiDescription Возвращает список пользователей-друзей онлайн.
 *
 * @apiParam {Number} idAccount Идентификатор пользователя.
 *
 * @apiPermission todo
 *
 *
 */
router.get('/getOnline/', function(req, res){
    res.send('getOnline');
});


module.exports = router;
