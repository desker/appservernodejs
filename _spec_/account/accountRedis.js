/**
 * Содержит информацию об аккаунта:
 *      - name - имя пользователя
 *      - ... - возможно и другие поля
 * Храним в Redis
 * Ключ: {account}:_id_account_
 * Тип: Hashes
 *
 * Почему Redis?
 * Информация об имени пользователя будет использоваться очень часто при выводе комментариев, сообщений и др.
 */
var accountRedis = {


    /**
     * В входящем массиве заполнит поле accountName
     * @param arItems
     */
    fillAccountName: function(arItems){

        //1. получаем уникальный массив всех idAccount

        //2. Получаем имена
        //редис может находиться на другой машине, поэтому нужно делать не разными запросами, а одним Pipelining или использовать Lua

        //3. Присваеваем
        arItems[0].accountName = 'Имя';

        return arItems;

    },

    /**
     * Возвращает имя пользователя по id
     * @param nAccountId
     * @returns {string}
     */
    getName: function(nAccountId){
        return "Вася"
    },

    /**
     * Установить имя пользователя
     * @param nAccountId
     * @param sName
     */
    setName: function(nAccountId, sName){

    }






};

module.exports = accountRedis;
