/*
Общие замечания.

Древовидные. 3 уровня.
Выводом в виде дерева будет заниматься UI на клиенте, т.е. от сервера не требуется ни каких действий, кроме как хранить parentId.

Модуль Комментарии может использоваться для комментирования любых объектов.
Например, сообщений, фотографий, статей.
Для этого вводиться понятие канал (chanel), в разрезе которых происходит комментирование.
Канал состоит из двух переменных: entity_type и entity_id
Возможные варианты entity_type в текущем проекте "post", "article", но могут быть добавлены новые.

Не путать entity_type комментариев с сообщениями блога.

Для всех классов написать тесты!

 */

/*
Класс отвечает за хранение комментариев в MySql
 */
var commentsMySql = require('./commentsMySql');

/*
Класс отвечает за хранение всех полей комментария в кэше Redis as LRU
 */
var commentsCache = require('./commentsCache');

/*
Класс отвечает за хранения списка комментариев в разрезе каналов.
 */
var commentsOfChanelRedis = require('./commentsOfChanelRedis');


/*
Класс подсчета количества комментариев в канале.
 */
var commentsCountRedis = require('./commentsCountRedis');



/*
 Mock object
 Текст комментариев будет парситься при попадании в кэш commentsCache. Например, из markdown в html.
 В ТЗ не входит.
 */
var messageParser = function(oItem){
    return oItem.message;
};

/*=============================================
 end
 =============================================*/





var comments = {


    /**
     * Добавить комментарий в канал
     * @param sEntityType
     * @param nEntityId
     * @param nAccountId
     * @param sMessage
     * @param parentId
     * @param arMentions - массив idAccount, которых упомянули в комментарии
     */


    add : function(sEntityType, nEntityId, nAccountId, sMessage, parentId, arMentions){


        //1. Добавляем комментарий в mysql
        var lComment = commentsMySql.add(sEntityType, nEntityId, nAccountId, parentId);

        //2. Добавим комментарий в кэш
        commentsCache.add(lComment.id, lComment, messageParser);

        //3. Установим счетчик комментариев
        var lCount = commentsCountRedis.incrementCount();


        //4. Добавляем комментарий в commentsOfChanelRedis
        commentsOfChanelRedis.add(sEntityType, nEntityId, lComment.id);



    },

    /**
     * Изменить сообщение в комментарии
     * @param sEntityType
     * @param nEntityId
     * @param nAccountId
     * @param nCommentId
     * @param sMessage
     * @param arRoles - роли пользователя. Например, может быть admin, user, moderator или их сочетание. Вынести их значения в константы
     */
    update : function(sEntityType, nEntityId, nAccountId, nCommentId, sMessage, arRoles, arMentions){


        //1. Добавляем комментарий в mysql
        var lComment = commentsMySql.update(sEntityType, nEntityId, nAccountId, sMessage, arRoles);

        //2. Добавим комментарий в кэш
        commentsCache.update(lComment.id, lComment, messageParser);


    },

    /**
     * Удаление комментария
     * @param sEntityType
     * @param nEntityId
     * @param nCommentId
     * @param arRoles - роли пользователя. Например, может быть admin, user, moderator или их сочетание. Вынести их значения в константы
     */
    deleteComment: function(sEntityType, nEntityId, nAccountId, nCommentId, arRoles){

        //1. Удаляем комментарий из mysql

        commentsMySql.deleteComment(nCommentId, arRoles);

        //2. Удаляем комментарий из кэша
        commentsCache.deleteComment(nCommentId);

        //3. Установим счетчик комментариев
        var lCount = commentsCountRedis.decrementCount();

        //4. Удалим комментарий из commentsOfChanelRedis
        commentsOfChanelRedis.deleteComments(sEntityType, nEntityId, nCommentId);

    },


    /**
     * Получить последние 3 комментария getList(sChanel, 3, 0)
     * Получить 10 комментариев после первых 3-ех getList(sChanel, 10, 3)
     * @param sEntityType
     * @param nEntityId
     * @param nCount - количество комментриев, которое нужно получить
     * @param nOffset - смещение
     * @returns {Array}
     */
    getList: function(sEntityType, nEntityId, nCount, nOffset){


        //1. Получим список id в канале
        var arCommentsList = commentsOfChanelRedis.getList(sEntityType, nEntityId, nCount, nOffset);

        //2. Получим информацию о комментариях (все поля) из кэша
        arCommentsList = commentsCache.getList(arCommentsList);

        //3. Получим информацию о тех комментариях, которых нет в кэше из mysql
        arCommentsList = commentsMySql.getList(arCommentsList, messageParser);

        //4. Положим те комментарии, которых не было в кэше в кэш, т.е. у котрых , _fromCache_!== true
        arCommentsList = commentsCache.setList(arCommentsList);

        return arCommentsList;


    },

    /**
     * Возвращает количество комментариев в канале
     * @param sEntityType
     * @param nEntityId
     * @returns {*}
     */
    getCount: function(sEntityType, nEntityId){
        return commentsCountRedis.getCount();
    }



};

module.exports = comments;
