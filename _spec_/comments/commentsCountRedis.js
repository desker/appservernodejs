/**
 * Храним в Redis
 * Таблица redis.commentsCount
 * Ключ: {commentsCount}:_id_chanel
 * Тип: strings
 */

/**
 * Возможно стоит упразнить commentsCountRedis, т.к. вся эта информация есть в commentsOfChanelRedis
 *
 */
var commentsCountRedis = {


    /**
     * Установить количество комментариев в канале
     * @param sChanel
     * @param nCount
     */
    setCount : function(sChanel, nCount){


    },

    getCount : function(sChanel){


    },

    /**
     * Увеличиваем счетчик
     * @param sChanel
     */
    incrementCount: function(sChanel){
        return 0;//новое количество комментариев
    },

    /**
     * Уменьшаем счетчик счетчик
     * @param sChanel
     */
    decrementCount: function(sChanel){

    },

    /**
     * Удалить канал
     * @param sChanel
     */
    deleteChanel: function(sChanel){

    }

};

module.exports = commentsCountRedis;
