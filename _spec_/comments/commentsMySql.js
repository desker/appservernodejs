/*

Класс отвечает за хранение комментариев в MySql

 CREATE TABLE test.comments (
 id int(18) NOT NULL AUTO_INCREMENT,
 entity_type varchar(30) NOT NULL,
 entity_id int(18) NOT NULL,
 accountId int(18) NOT NULL COMMENT 'Автор комментария',
 parentId int(18) DEFAULT NULL COMMENT 'id комментария, на который дан ответ',
 message tinytext NOT NULL COMMENT 'Текст комментария',
 createdAt datetime DEFAULT NULL,
 likePos int(6) DEFAULT NULL COMMENT 'Количество лайков положительных',
 likeNeg int(6) DEFAULT NULL COMMENT 'Количество негативных лайков',
 PRIMARY KEY (id)
 )
 ENGINE = INNODB

 //todo определиться с индексами


 */
var commentsMySql = {


    /**
     * Добавить комментарий
     * @param sEntityType
     * @param nEntityId
     * @param nAccountId
     * @param sMessage
     * @param parentId
     * @param oOptions
     */
    add : function(sEntityType, nEntityId, nAccountId, sMessage, parentId, createdAt /*Если createdAt не передан, то устанавливается дата сервера*/){

        //вставляем в mysql

        /**
         * Возвращает объект со всеми полями + id
         */
        return {};

    },

    /**
     * Обновить текст комментария
     * @param sMessage
     */
    update: function(sEntityType, nEntityId,  nAccountId, sMessage, arRoles){
        /*
         acl проверяем:
         Если arRoles=user только автор может изменить комментарий, т.е. проверяем nAccountId
         Если arRoles=admin or moderator, тоже может изменить комментарий
         Если нет прав, то вовзращает описание ошибки.
         */
    },

    /**
     * Удалить комментарий.
     * @param nCommentId
     */
    deleteComment: function(nCommentId){
        /*
         acl проверяем:
         Если arRoles=user только автор может удалить комментарий, т.е. проверяем nAccountId
         Если arRoles=admin or moderator, тоже может удалить комментарий
         Если нет прав, то вовзращает описание ошибки.
         */
    },

    /**
     * Возвращаем всю информацию (все поля) из БД для переданных id комментариев
     * @param arCommentsIds
     */
    getList: function(arCommentsIds, messageParser){

        var lResult = [];

        /*
        Если item arCommentsIds isObject, то пропустить
         */

        if(messageParser){
            //применим парсер
            lResult.forEach(function(oItem, i){
                oItem.messageParse = messageParser(oItem);

            });
        }

        //заполним accountName
        accountRedis.fillAccountName(lResult);
        return lResult;
    }

    ///**
    // * Возвращает количество комментариев в канале
    // * @param sChanel
    // * @param nCount
    // */
    //getCount : function(sChanel){
    //
    //
    //}

};

module.exports = commentsMySql;
