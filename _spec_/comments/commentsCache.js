/*
 Класс отвечает за хранение всех полей комментария в кэше Redis as LRU
 Храним в Redis as an LRU cache

 Ключ: {comments}:_id_comment_
 Тип: Hashes
 Время хранения в кэше 2 часа. Задется в config

 Какие поля содержит:
    Эти поля из commentsMySql
    - id,
    - entityType,
    - entityId
    - accountId
    - parentId
    - message
    - likePos
    - likeNeg
    - createdAt
    Также добавляются
    - accountName  см. accountRedis
    - messageParse

 */

var accountRedis = require('../account/accountRedis');

var commentsCache = {


    /**
     * Добавить комментарий в кэш
     * @param nCommentId
     * @param oData
     * @param messageParser
     */
    add : function(nCommentId, oData, messageParser){

        oData.messageParse = messageParser(oData);
        oData.accountName = accountRedis.getName(oData.accountId);

    },

    /**
     * Обновить текст комментария
     * @param nCommentId
     * @param sMessage
     */
    update: function(nCommentId, oData, messageParser){
        oData.messageParse = messageParser(oData);
    },

    /**
     * Удалить комментарий из кэша.
     * @param nCommentId
     */
    deleteComment: function(nCommentId){

    },

    /**
     * Положим те комментарии, которых не было в кэше в кэш, т.е. у котрых , _fromCache_!== true
     * @param arComments
     */
    setList: function(arComments){

        //Pipelining

        return;
    },

    /**
     * Возвращаем всю информацию (все поля) из кэша для переданных id комментариев
     * Использовать Pipelining
     * @param arCommentsIds
     */
    getList: function(arCommentsIds){
        /*
         Например:
         arCommentsIds = [1,4,5, 6, 7]
         Возвращает: [
             //Те комментарии, которые есть в кэше
             {id:1, ..., _fromCache_: true},
             {id:4, ..., _fromCache_: true},
             //Те комментарии, котрых нет в кэше
             5,
             6,
             7
         ]
         */

        /*
        К строкам добавить _fromCache_= true
         */
        return [];
    }

};

module.exports = commentsCache;
