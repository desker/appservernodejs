/**
 * Класс отвечает за хранения списка комментариев в разрезе каналов.
 * Храним в Redis
 * Ключ: {comments-chanel}:_chanel_id_
 * Тип: sorted sets
 * Используется для пагинации: получения выборки комментариев.
 */
var commentsOfChanelRedis = {


    /**
     * Добавить комментарий в список
     * @param sEntityType
     * @param nEntityId
     * @param nCommentId
     */
    add : function(sEntityType, nEntityId, nCommentId){

        /**
         * Добавляем функцией ZINCRBY. В этом случае порядок будет такой: в начале списка новые, а в конце старые.
         */

    },

    /**
     * Удалить комментарий из списка
     * @param sChanel
     * @param nCommentId
     */
    deleteComments : function(sChanel, nCommentId){


    },

    /**
     * Возвращает последние nCount комментариев канала. offset - смещение
     * @param sChanel
     * @param nCount
     * @param nOffset
     */
    getList: function(sChanel, nCount, nOffset){

    }
    //
    ///**
    // * Количество комментариев канале
    // * @param sChanel
    // */
    //getCount : function(sChanel){
    //
    //
    //}


};

module.exports = commentsOfChanelRedis;
